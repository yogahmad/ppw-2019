from django.apps import AppConfig


class ProfilpageConfig(AppConfig):
    name = 'profilpage'
